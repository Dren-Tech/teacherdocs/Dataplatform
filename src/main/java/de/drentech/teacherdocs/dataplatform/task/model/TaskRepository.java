package de.drentech.teacherdocs.dataplatform.task.model;

import com.github.javafaker.Faker;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class TaskRepository {

    @Inject
    EntityManager em;

    public List<Task> findAll() {
        Query query = em.createQuery("SELECT t FROM Task t");

        List<Task> tasks = query.getResultList();

        return tasks;
    }

    @Transactional
    public Task addRandomTask() {
        Faker faker = new Faker();

        Task task = new Task();

        task.setTitle(faker.book().title());
        task.setDescription(faker.hitchhikersGuideToTheGalaxy().marvinQuote());

        this.em.persist(task);

        return task;
    }
}
