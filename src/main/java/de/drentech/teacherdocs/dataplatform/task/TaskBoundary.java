package de.drentech.teacherdocs.dataplatform.task;

import de.drentech.teacherdocs.dataplatform.task.model.Task;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Timed;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;
import java.util.List;
import java.util.Random;

@Path("/task")
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class TaskBoundary {
    @Inject
    private TaskFascade taskFascade;

    @GET
    @Timed(unit = MetricUnits.MILLISECONDS)
    public List<Task> getAllTasks() {
        return this.taskFascade.getAllTasks();
    }

    @GET
    @Path("/create-new")
    public Task createNewTask() {
        return this.taskFascade.randomTask();
    }
}
