package de.drentech.teacherdocs.dataplatform.task.model;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class Task implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Type(type="pg-uuid")
    public UUID id;

    public String title;

    public String description;

    public Task() {

    }

    public Task(String title, String description) {
        this.id = UUID.randomUUID();

        this.title = title;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Task{" +
                "uuid=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
