package de.drentech.teacherdocs.dataplatform.task;

import de.drentech.teacherdocs.dataplatform.task.model.Task;
import de.drentech.teacherdocs.dataplatform.task.model.TaskRepository;
import org.eclipse.microprofile.metrics.annotation.Counted;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class TaskFascade {

    @Inject
    private TaskRepository taskRepository;

    @Counted(monotonic = true)
    public List<Task> getAllTasks() {
        return this.taskRepository.findAll();
    }

    public Task randomTask() {
        return this.taskRepository.addRandomTask();
    }
}
